# Documents per la implementació de Project Based Learning

<details><summary>[versió en català]</summary>


Es comparteixen els documents generats per la implementació de la metodologia docent _Project Based Learning_ a les assignatures d'Informàtica de la Facultat de Nàutica, de la Universitat Politècnica de Catalunya. 

S'inclou:

- Objectius d'aprenentatge (pdf i odt)
- Rúbrica d'avaluació (pdf i docx)
- Autoavaluació de coneixements, test kpsi (pdf i odt)
- Model diari d'aprenentatge (pdf i odt)
- Enquesta de satisfacció per l'alumnat (pdf i docx)
- Programa de sessions (pdf i odt)

Les versions en català i castellà es troben a les carpetes CAT i ESP respectivament. 

L'experiència docent completa es presenta a les XXVII Jornadas sobre la Enseñanza Universitaria de la Informática (JENUI 2021). 

</details>

# Documentos para la implementación de Project Based Learning

<details><summary>[versión en castellano]</summary>


Se comparten los documentos generados durante la implementación de la metodología docente _Project Based Learning_ a las asignaturas de Informática de la Facultat de Nàutica, de la Universitat Politècnica de Catalunya. 

Se incluye:

- Objetivos de aprendizaje (pdf y odt)
- Rúbrica de evaluación (pdf y docx)
- Autoevaluación de conocimientos, test kpsi (pdf y odt)
- Modelo diario de aprendizaje (pdf y odt)
- Encuesta de satisfacción para el alumnado (pdf y docx)
- Programa de sesiones (pdf y odt)

Las versiones en castellano y catalán se encuentran dentro las carpetas ESP y CAT respectivamente. 

La experiencia docente completa se presenta en las XXVII Jornadas sobre la Enseñanza Universitaria de la Informática (JENUI 2021).

</details>
